package com.muhardin.endy.belajar.clientwebserverside.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/transaksi")
public class TransaksiController {

    @GetMapping("/list")
    public ModelMap dataTransaksi(){
        return new ModelMap();
    }

}
