package com.muhardin.endy.belajar.resourceserver.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class Transaksi {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private LocalDateTime waktuTransaksi;

    @NotEmpty
    private String kodeTransaksi;

    private String keterangan;

    @NotNull @Min(1)
    private BigDecimal nilai;

    @NotEmpty
    private String idUserCreate;
    private String idUserApprove;
}
