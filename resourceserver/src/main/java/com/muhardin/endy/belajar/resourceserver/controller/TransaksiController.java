package com.muhardin.endy.belajar.resourceserver.controller;

import com.muhardin.endy.belajar.resourceserver.dao.TransaksiDao;
import com.muhardin.endy.belajar.resourceserver.entity.Transaksi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransaksiController {
    @Autowired private TransaksiDao transaksiDao;

    @GetMapping("/transaksi/")
    public Page<Transaksi> dataTransaksi(Pageable page) {
        return transaksiDao.findAll(page);
    }
}