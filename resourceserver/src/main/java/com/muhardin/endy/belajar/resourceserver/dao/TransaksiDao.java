package com.muhardin.endy.belajar.resourceserver.dao;

import com.muhardin.endy.belajar.resourceserver.entity.Transaksi;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransaksiDao extends PagingAndSortingRepository<Transaksi, String> {
}
