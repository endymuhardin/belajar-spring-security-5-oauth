create table transaksi (
  id varchar(36),
  waktu_transaksi datetime not null,
  kode_transaksi varchar(100) not null,
  keterangan varchar(255),
  nilai decimal(19,2) not null,
  id_user_create varchar(36) not null,
  id_user_approve varchar(36),
  primary key (id),
  unique (kode_transaksi)
);