# Authorization Server #

* Authorization URL : http://localhost:10001/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code

* Tukar `code` menjadi `access_token`

    ```
     curl -X POST \
         -H "Accept: application/json" \
         -u "clientwebbased:abcd" \
         -d "client_id=clientwebbased&grant_type=authorization_code&code=85DeyK" \
         http://localhost:10001/oauth/token
    ```
    
    Hasilnya :
    
    ```json
    {
      "access_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiZW50cmlfZGF0YSIsInJldmlld190cmFuc2Frc2kiLCJhcHByb3ZlX3RyYW5zYWtzaSJdLCJleHAiOjE1NTM3MzUyNzAsImF1dGhvcml0aWVzIjpbIlZJRVdfVFJBTlNBS1NJIl0sImp0aSI6ImFkN2Q5MTliLWMwMmUtNGZiNy04YzNhLTEwOTU2ZTg1MmM0MCIsImNsaWVudF9pZCI6ImNsaWVudHdlYmJhc2VkIn0.BE0tMs7Tr7GmYo4FpYYw3MV6fUeHcOMIJzNS69TjJmS0t8OWMzxa5UU635BvXDrtJc5-59AYwwXnyA9RFpzWM3f5vMn4Sl2oMw7eh6ZZOkZaglruyeu1kCTYfx6DmeFyyeaOJ5mjkvhqbBze7oWVXU3aVD9xadv1Ggcy_86XyCEUijGPeU5CJ75KFb-_xJGy-10a15W2wOXzeBJ-nXFTfgA7Dmu7CxQt_JeQyM2PIx0wT3wQDp_m2aG9Pb9_tkF4nKlDglKQAe3Ef3AYuHvud8umF7SDho0kYFYjfESJWAXE0cQ4PIox4hx235dstjPXyDietqRiRW35Jyj80QPZLA",
      "token_type":"bearer",
      "refresh_token":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiZW50cmlfZGF0YSIsInJldmlld190cmFuc2Frc2kiLCJhcHByb3ZlX3RyYW5zYWtzaSJdLCJhdGkiOiJhZDdkOTE5Yi1jMDJlLTRmYjctOGMzYS0xMDk1NmU4NTJjNDAiLCJleHAiOjE1NTYyODQwNzAsImF1dGhvcml0aWVzIjpbIlZJRVdfVFJBTlNBS1NJIl0sImp0aSI6ImVjODIwYTlkLTA1ZTctNDhmMS1hOWU2LWY0Mjg2YTQ1ZWFmYiIsImNsaWVudF9pZCI6ImNsaWVudHdlYmJhc2VkIn0.O6eN2y8jTfY_1FUlqQ-erzVarPtD6fmu3vodnJpm6PFUQC0cERhjlJ9-9xC4HPMEYKaGWP1od6Ac0tCGTDTGyz-WFgbJSN7y-z8NwVQXkWv3XLU72R7sWhxlP1V7vDOEsqqrzFCozOrocJWo1CeMvFyuttMEVkPunbxGrqUj6YEIWcnOT-JET3-W9Vlxov3o16dXLPODdgTUU4uO4MlEtDzs9PWXvWipO2q2K6KB-UfKcHbzGLLbZQUuA-40iyOmZDFleRc-hd6Rp1iSSZrC7bitPPyB_lDJ_r3HSRBjVCD5pPfpIA845O-WqlRe-NfDt2UFcwu2jKMlJT6pQ_yxNg","expires_in":43199,"scope":"entri_data review_transaksi approve_transaksi","jti":"ad7d919b-c02e-4fb7-8c3a-10956e852c40",
    }
    ```

* Mengakses Resource Server

    ```
    curl -X GET \
         -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiZW50cmlfZGF0YSIsInJldmlld190cmFuc2Frc2kiLCJhcHByb3ZlX3RyYW5zYWtzaSJdLCJleHAiOjE1NTM3MzUyNzAsImF1dGhvcml0aWVzIjpbIlZJRVdfVFJBTlNBS1NJIl0sImp0aSI6ImFkN2Q5MTliLWMwMmUtNGZiNy04YzNhLTEwOTU2ZTg1MmM0MCIsImNsaWVudF9pZCI6ImNsaWVudHdlYmJhc2VkIn0.BE0tMs7Tr7GmYo4FpYYw3MV6fUeHcOMIJzNS69TjJmS0t8OWMzxa5UU635BvXDrtJc5-59AYwwXnyA9RFpzWM3f5vMn4Sl2oMw7eh6ZZOkZaglruyeu1kCTYfx6DmeFyyeaOJ5mjkvhqbBze7oWVXU3aVD9xadv1Ggcy_86XyCEUijGPeU5CJ75KFb-_xJGy-10a15W2wOXzeBJ-nXFTfgA7Dmu7CxQt_JeQyM2PIx0wT3wQDp_m2aG9Pb9_tkF4nKlDglKQAe3Ef3AYuHvud8umF7SDho0kYFYjfESJWAXE0cQ4PIox4hx235dstjPXyDietqRiRW35Jyj80QPZLA" \
         http://localhost:10003/transaksi/
    ```
    
    Hasilnya :
    
    ```json
    {
        "content": [
            {
                "id": "t001",
                "waktuTransaksi": "2019-03-01T08:00:01",
                "kodeTransaksi": "T-001",
                "keterangan": "Transaksi 001",
                "nilai": 100001,
                "idUserCreate": "u001",
                "idUserApprove": null
            },
            {
                "id": "t002",
                "waktuTransaksi": "2019-03-02T08:00:02",
                "kodeTransaksi": "T-002",
                "keterangan": "Transaksi 002",
                "nilai": 100002,
                "idUserCreate": "u002",
                "idUserApprove": null
            },
            {
                "id": "t003",
                "waktuTransaksi": "2019-03-03T08:00:03",
                "kodeTransaksi": "T-003",
                "keterangan": "Transaksi 003",
                "nilai": 100002,
                "idUserCreate": "u001",
                "idUserApprove": null
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalPages": 1,
        "totalElements": 3,
        "last": true,
        "size": 20,
        "number": 0,
        "numberOfElements": 3,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "first": true,
        "empty": false
    }
    ```